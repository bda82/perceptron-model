import numpy as np
from sklearn import datasets
import matplotlib.pyplot as plt

# Создание набора данных
X, y = datasets.make_blobs(n_samples=150,
                           n_features=2,
                           centers=2,
                           cluster_std=1.05,
                           random_state=2)
# Вывод графика № 1
fig = plt.figure(figsize=(10,8))
plt.plot(X[:, 0][y == 0], X[:, 1][y == 0], 'r^')
plt.plot(X[:, 0][y == 1], X[:, 1][y == 1], 'bs')
plt.xlabel("особенность 1")
plt.ylabel("особенность 2")
plt.title('Исходные данные')
plt.show()

def step_func(z):
    return 1.0 if (z > 0) else 0.0


def perceptron(X, y, lr, epochs):
    # X --> Входы
    # y --> Выходы (метки, центры)
    # lr --> Скорость обучения
    # epochs --> Количество итераций алгоритма
    
    # m-> Количество примеров для обучения
    # n-> Количество особенностей
    m, n = X.shape
    
    # Инициализация параметров (тета) в нули
    # +1 в n+1 для срока смещения
    theta = np.zeros((n+1, 1))
    
    # Пустой список для хранения того, сколько примеров 
    # неправильно классифицируется на каждой итерации
    n_miss_list = []
    
    # Обучение
    for epoch in range(epochs):
        # переменная для хранения образца, 
        # который неправильно классифицирован (#misclassified)
        n_miss = 0
        
        # Итерация по каждомо образцу
        for idx, x_i in enumerate(X):
            
            # Вставляем 1 для смещения (bias), X0 = 1.
            x_i = np.insert(x_i, 0, 1).reshape(-1,1)
            
            # Расчет гипотезы/предсказания
            y_hat = step_func(np.dot(x_i.T, theta))
            
            # Обновление если пример неправильно классифицирован
            if (np.squeeze(y_hat) - y[idx]) != 0:
                theta += lr*((y[idx] - y_hat)*x_i)
                n_miss += 1
        
        # Добавление к списку неправильно классифицированных
        # на каждой итерации
        n_miss_list.append(n_miss)
        
    return theta, n_miss_list


def plot_decision_boundary(X, theta):
    # X --> Входы
    # theta --> Параметры
    
    # Линия разделения по уравнению y = mx + c
    # Так что, вычисляем: mx + c = theta0.X0 + theta1.X1 + theta2.X2
    # Решаем уравление для нахождения m и c
    x1 = [min(X[:,0]), max(X[:,0])]
    m = -theta[1]/theta[2]
    c = -theta[0]/theta[2]
    x2 = m*x1 + c
    
    # Вывод графика № 2
    fig = plt.figure(figsize=(10,8))
    plt.plot(X[:, 0][y==0], X[:, 1][y==0], "r^")
    plt.plot(X[:, 0][y==1], X[:, 1][y==1], "bs")
    plt.xlabel("Особенность 1")
    plt.ylabel("особенность 2")
    plt.title('Работа перцептрона')
    plt.plot(x1, x2, 'y-')
    plt.show()
    
    
theta, miss_l = perceptron(X, y, 0.5, 100)
plot_decision_boundary(X, theta)
